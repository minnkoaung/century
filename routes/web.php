<?php
//get
Route::get("/", 'FrontPageController@index')->name("home");
Route::get("/index.html", 'FrontPageController@index');
//Route::get('/blog','FrontPageController@blog')->name('blog');

//post
Route::post('/booking','BookingController@sendBooking')->name('send_booking');
Route::post('/contact', 'ContactController@send')->name("sendcontact");
//auth
Auth::routes();
Route::group(['middleware' => ['web', 'auth']], function () {
	Route::get('/backend', 'PostController@dashboard')->name('dashboard');
	Route::resource('backend/meta', 'MetaController');
	Route::resource('backend/cars', 'CarController');
	Route::resource('backend/team_members', 'TeamMemberController');
	//Route::resource('backend/posts', 'ArticleController');
	//Articles
    Route::get('backend/posts','ArticleController@index')->name("posts.index");
    Route::get("backend/posts/create", 'ArticleController@create')->name('posts.create');
    Route::post("backend/posts/store",'ArticleController@store')->name("posts.store");
    Route::get("backend/posts/data", 'ArticleController@data')->name("posts.data");
    Route::get('backend/posts/{id}/edit',"ArticleController@edit")->name('posts.edit');
    Route::get('backend/posts/show/{id}', 'ArticleController@show')->name('posts.show');
    Route::put('backend/posts/update/{id}',"ArticleController@update")->name('posts.update');
	Route::delete('backend/posts/delete/{id}',"ArticleController@destroy")->name('posts.delete');
	//Clients
    Route::get('backend/clients','ClientController@index')->name("clients.index");
    Route::get("backend/clients/create", 'ClientController@create')->name('clients.create');
    Route::post("backend/clients/store",'ClientController@store')->name("clients.store");
    Route::get("backend/clients/data", 'ClientController@data')->name("clients.data");
    Route::get('backend/clients/{id}/edit',"ClientController@edit")->name('clients.edit');
    Route::get('backend/clients/show/{id}', 'ClientController@show')->name('clients.show');
    Route::put('backend/clients/update/{id}',"ClientController@update")->name('clients.update');
    Route::delete('backend/clients/delete/{id}',"ClientController@destroy")->name('clients.delete');
    //Category
	Route::resource('backend/categories', 'CategoryController');
	//Contact Listing
	Route::get('backend/contact-listing', 'ContactController@index')->name('contact-list');
	//Route::get('/contact-listing', 'ContactController@index')->name("contact-list");
	Route::delete('backend/contact/{id}', 'ContactController@destroy')->name('contact-destory');
});


