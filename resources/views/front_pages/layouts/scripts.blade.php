<script src=js/jquery.js></script>
  <script>
      window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js">
  </script>')
<script src={{ asset("js/bootstrap.min.js")}}></script>
  <script src={{ asset("js/waypoints.min.js")}}></script>
  <script src={{ asset("js/bar.js")}}></script>
  <script src={{ asset("js/owl.carousel.min.js")}}></script>
  <script src={{ asset("js/jquery.counterup.min.js")}}></script>
  <script src={{asset("js/main.js")}}></script>
  <script>
      ! function(e, t, a, n, c, o, s) {
          e.GoogleAnalyticsObject = c, e[c] = e[c] || function() {
                  (e[c].q = e[c].q || []).push(arguments)
              }, e[c].l = 1 * new Date, o = t.createElement(a), s = t.getElementsByTagName(a)[0], o.async = 1,
              o.src = n, s.parentNode.insertBefore(o, s)
      }(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga"), ga("create",
          "UA-90183414-1", "auto"), ga("send", "pageview")
  </script>
  <script>
      window.fbAsyncInit = function() {
              FB.init({
                  appId: "411148002561966",
                  xfbml: !0,
                  version: "v2.8"
              }), FB.AppEvents.logPageView()
          },
          function(e, n, t) {
              var o, c = e.getElementsByTagName(n)[0];
              e.getElementById(t) || (o = e.createElement(n), o.id = t, o.src =
                  "//connect.facebook.net/en_US/sdk.js", c.parentNode.insertBefore(o, c))
          }(document, "script", "facebook-jssdk")
  </script>