<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
<!-- PRE LOADER -->
<div class="preloader">
  <div class="cssload-dots">
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
  </div>
</div>
<!-- Navigation Section -->
<div class="navbar custom-navbar wow fadeInDown" data-wow-duration="2s" role="navigation" id="header">
  <div class="container">
    <!-- NAVBAR HEADER -->
    <div class="navbar-header">
      <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon icon-bar"></span>
        <span class="icon icon-bar"></span>
        <span class="icon icon-bar"></span> </button>
      <!-- lOGO TEXT HERE -->
      <a href="/" class="navbar-brand hidden-xs"><img class="nav-brand-pic" src="images/onTrackNewLogo.png" alt="" style="max-width: 260px;margin-top: -25px;"> </a></div>

    <!-- NAVIGATION LINKS -->
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#slider" class="smoothScroll">Home</a></li>
        <li><a href="#about" class="smoothScroll">About Us</a></li>
        <li><a href="#cars" class="smoothScroll">Our Fleet</a></li>
        <li><a href="#service" class="smoothScroll">Services</a></li>
        <!-- <li><a href="{{ url('blog')}}" class="">Blog</a></li> -->
       <!--  <li><a href="#testimonials" class="smoothScroll">Clients</a></li> -->
        <li><a href="#contact" class="smoothScroll">Contact Us</a></li>
        <li><span class="calltxt"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+959777488458" style="color: #fff;">+959 777 488 458</a></span></li>
      </ul>
    </div>
  </div>
</div>