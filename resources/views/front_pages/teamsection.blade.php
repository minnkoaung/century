<div class="section team_section" id=team_section>
    <div class=container>
        <div class=row>
            <div class=team_area>
                <h1 class=title>The Team</h1>
                <h3 class=sub_title>Behind Our Success</h3>
                <div class="col-md-3 team">
                    <a href="" class="pull-left text-center single_team">
                        <div class=front>
                            <img src="{{ asset("images/team1.jpg")}}" class=img-responsive>                                <span>Minn Ko Aung</span></div>
                        <div class=behind>Creative Director</div>
                    </a>
                    <ul class=socials_link>
                        <li><a href=https://www.facebook.com/me.minnkoaung><i class="fa fa-facebook-square"aria-hidden=true></i></a>
                            <li><a href=https://twitter.com/Minnkoaung><i class="fa fa-twitter-square"aria-hidden=true></i></a>
                                <li>
                                    <a href=https://www.linkedin.com/in/minnkoaung><i class="fa fa-linkedin-square" aria-hidden=true></i></a>
                    </ul>
                </div>
                <div class="col-md-3 team">
                    <a href="" class="pull-left text-center">
                    <div class=front><img src={{ asset("images/team1.jpg")}} class=img-responsive> <span>Hnin Wai Wai Aung</span></div>
                        <div class=behind>Marketing Manager</div>
                    </a>
                    <ul class=socials_link>
                        <li><a href=#><i class="fa fa-facebook-square"aria-hidden=true></i></a>
                            <li><a href=#><i class="fa fa-twitter-square"aria-hidden=true></i></a>
                                <li><a href=#><i class="fa fa-linkedin-square"aria-hidden=true></i></a></ul>
                </div>
                
                
                <div class="col-md-3 team">
                    <a href="" class="pull-left text-center single_team">
                        <div class=front><img src=images/team1.jpg class=img-responsive> <span>Kyaw Oo</span></div>
                        <div class=behind>Sr.Graphic Designer</div>
                    </a>
                    <ul class=socials_link>
                        <li><a href=#><i class="fa fa-facebook-square"aria-hidden=true></i></a>
                            <li><a href=#><i class="fa fa-twitter-square"aria-hidden=true></i></a>
                                <li><a href=#><i class="fa fa-linkedin-square"aria-hidden=true></i></a></ul>
                </div>
                
            </div>
        </div>
    </div>
</div>