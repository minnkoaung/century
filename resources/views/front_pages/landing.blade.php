@include('front_pages.layouts.header')
<body class=scroll-area data-offset=90 data-spy=scroll>
  <div id=preloader>
      <div id=status>
          <p style=text-align:center>loading...</div>
  </div>
  <nav class="navbar navbar-default navbar-fixed-top" id=mainNav>
      <div class=container>
          <div class=navbar-header><button class="collapsed navbar-toggle" type=button data-target=#bs-example-navbar-collapse-1 data-toggle=collapse><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button>
              <a href=# class=navbar-brand><img src=images/century-badge-200x50.png alt="Century Logo"></a>
          </div>
          <div class="collapse navbar-collapse" id=bs-example-navbar-collapse-1>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href=#header_section>Home</a>
                      <li><a href=#about_section>About</a>
                          <li><a href=#service_section>Service</a>
                              <li><a href=#work_section>Work</a>
                                  <li><a href=#portfolio_section>Portfolio</a>
                                      <li><a href=#team_section>Team</a>
                                          <li><a href=#testimonial_section>Testimonial</a></ul>
          </div>
      </div>
  </nav>
  <div class="header_section jumbotron" id=header_section>
      <div class=container>
          <h3>A professional working team</h3>
          <h1 class=main_title style=color:#EC3F44>Build your idea with our inspirations</h1>
          <p class=title_desc>ကျွန်တော်တို့သည် Graphic Design နှင့် Digital Artworks များဖန်တီးမှုတွင် အတွေ့အကြုံ (၇) နှစ်နှင့် ရှိထားပြီးသော
          Professtional များဖြင့် စုဖွဲ့ထားသောအစုအဖွဲ့ဖြစ်ပါသည်။ လူကြီးမင်းတို့နှင့်ပူးပေါင်းကာ လူကြီးမင်းများလိုအပ်မည့် Digital Products များ ဖန်တီးမှုတွင် အကောင်းမွန်ဆုံး
        ကူညီပေးပါရစေ။</p><a href=# class="actv button_style">ကျွန်ုပ်တို့အကြောင်း</a></div>
  </div>
  <div class=buynow_section>
      <div class=container>
          <div class=row>
              <div class=buynow_area>
                  <div class="col-sm-12 col-xs-12 col-md-8 title_area">
                      <h1 style="line-height:70px;">လူကြီးမင်းတို့၏စီးပွားရေးဆိုင်ရာအချက်အလက်များ လုံခြုံမှုကို အထူးအာမခံပါသည်။</h1>
                  </div>
                  <div class="col-md-4 btn_area col-sm-12 col-xs-12" style="margin-top:60px;"><a href=# class=buy_btn>+959973494973</a></div>
              </div>
          </div>
      </div>
  </div>
  <div class="section about_section" id=about_section>
      <div class=container>
          <div class=row>
              <div class=about_area>
                  <h1 class=title>About Us</h1>
                  <h3 class=sub_title>All you need to know</h3>
                  <hr>
                  <div class="col-md-4 col-sm-4 single_about">
                      <a href=# data-target=.modal1 data-toggle=modal><img src={{ asset("images/about2.jpg")}}class="img-responsive sm_img"></a>
                      <h2>Best Resourse we have</h2>
                      <p>All our sevices, under the hands of professionals, produces great quality proudcts/ materials.
                          <div class="fade modal modal1" role=dialog aria-labelledby=myLargeModalLabel tabindex=-1>
                              <div class="modal-dialog modal-lg" role=document>
                                  <div class=modal-content><img src=images/about1.jpg class=img-responsive></div>
                              </div>
                          </div>
                  </div>
                  <div class="col-md-4 col-sm-4 single_about">
                      <a href=# class=small_screen data-target=.modal2 data-toggle=modal><img src=images/about1.jpg class="img-responsive sm_img"></a>
                      <h2>Best Affort We Provide</h2>
                      <p>We are a team of creative individuals with diverse yet complementary experiences and backgrounds who united over a shared love of design and desire to help others succeed.</p>
                  <a href=# class=large_screen data-target=.modal2 data-toggle=modal><img src={{ asset("images/about3.jpg")}} class="img-responsive sm_img"></a>
                      <div class="fade modal modal2" role=dialog aria-labelledby=myLargeModalLabel tabindex=-1>
                          <div class="modal-dialog modal-lg" role=document>
                              <div class=modal-content><img src=images/about2.jpg class=img-responsive></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-4 single_about">
                      <a href=# data-target=.modal3 data-toggle=modal><img src={{ asset("images/about2.jpg")}} class="img-responsive sm_img"></a>
                      <h2>Best Result We Bring</h2>
                      <p>We approach every project as a collaboration, and are most excited when we have the opportunity to learn from you. Our goal is to help you continue doing to get the best for you, while we work hard to do what we love.
                          <div class="fade modal modal3" role=dialog aria-labelledby=myLargeModalLabel tabindex=-1>
                              <div class="modal-dialog modal-lg" role=document>
                                  <div class=modal-content><img src=images/about3.jpg class=img-responsive></div>
                              </div>
                          </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="section service_section" id=service_section>
      <div class=container>
          <div class=row>
              <div class=service_area>
                  <div class=col-md-6>
                      <h3>Our Services</h3>
                      <h1>What Service we Provide</h1>
                      <hr>
                      <div>
                          <p>We offer graphic design and layout services at a very reasonable price.We also offer offset printing services for printing requirements. Our offset printing services, under the hands of experienced operators, produces great
                              quality printed materials.
                              <p>Design Services include:<br>* Brochures, newsletters, booklets, and book covers<br>* Identity and branding packages, including logos<br>* Special event materials: invitations, announcements, posters, programs, banners,
                                  etc.
                                  <br>* Custom pieces such as magnets and post-it pads<br>* Web graphics and custom HTML emails</div>
                  </div>
                  <div class=col-md-6>
                      <div class="col-sm-6 col-md-6 single_service"><i class="fa fa-line-chart" aria-hidden=true></i>
                          <h2>Digital Marketing</h2>
                      </div>
                      <div class="col-sm-6 col-md-6 single_service"><i class="fa fa-rocket" aria-hidden=true></i>
                          <h2>One-Stop Printing</h2>
                      </div>
                      <div class="col-sm-6 col-md-6 single_service"><i class="fa fa-star" aria-hidden=true></i>
                          <h2>Design Solutions</h2>
                      </div>
                      <div class="col-sm-6 col-md-6 single_service"><i class="fa fa-bolt" aria-hidden=true></i>
                          <h2>Software Solutions</h2>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="section work_section" id=work_section>
      <div class=container>
          <div class=row>
              <div class=work_area>
                  <div class="col-sm-6 col-md-6 work_right">
                      <h3>Work Experience</h3>
                      <h1>What Can We Do</h1>
                      <div class=bar_group>
                          <div class="bar_group__bar thin" label="Adobe Photoshop" show_values=true tooltip=true value=98></div>
                          <div class="bar_group__bar thin" label="Adobe Illustraor" show_values=true tooltip=true value=98></div>
                          <div class="bar_group__bar thin" label="Adobe Indesign" show_values=true tooltip=true value=98></div>
                          <div class="bar_group__bar thin" label=Front-End show_values=true tooltip=true value=95></div>
                          <div class="bar_group__bar thin" label=Back-End show_values=true tooltip=true value=85></div>
                          <div class="bar_group__bar thin" label=UI/UX show_values=true tooltip=true value=95></div>
                      </div>
                  </div>
                  <div class="col-sm-6 col-md-6 work_left" style=padding-top:45px><img src={{ asset("images/aboutus.jpg")}} class=img-responsive></div>
              </div>
          </div>
      </div>
  </div>
  <div class="section portfolio_section" id=portfolio_section>
      <div class=container>
          <div class=row>
              <div class=portfolio_area>
                  <h1 class=title>Our Work</h1>
                  <h3 class=sub_title>We make Creative things</h3>
                  <hr>
                  <div class="col-sm-6 col-md-4 thumb">
                      <a href=# data-target=.portfolio-modal1 data-toggle=modal><img src=images/item1.jpg class=img-responsive alt="">
                          <h3>Branding<i>+</i></h3>
                          <p>Development & research<br>Logo formation<br>Strategy & placement<br>Brand campaigns & application
                          </p>
                      </a>
                  </div>
                  <div class="fade modal portfolio-modal1" role=dialog aria-labelledby=portfolioModal tabindex=-1>
                      <div class="modal-dialog modal-lg" role=document>
                          <div class="modal-content portfolio-modal">
                              <div class=modal-body>
                                  <div class=row><button class=close type=button aria-label=Close data-dismiss=modal><span aria-hidden=true>×</span></button>
                                      <div class="col-sm-6 col-md-6 col-xs-12 modal_img"><img src=images/item1.jpg class=img-responsive></div>
                                      <div class="col-sm-6 col-md-6 col-xs-12">
                                          <h2>Branding</h2>
                                          <p>Development & research<br>Logo formation<br>Strategy & placement<br>Brand campaigns & application</div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-6 col-md-4 thumb">
                      <a href=# data-target=.portfolio-modal2 data-toggle=modal><img src=images/item2.jpg class=img-responsive alt="">
                          <h3>Digital Services<i>+</i></h3>
                          <p>Stationery & promotional<br>Website design & build<br>Digital marketing<br>E-commerce</p>
                      </a>
                  </div>
                  <div class="fade modal portfolio-modal2" role=dialog aria-labelledby=portfolioModal tabindex=-1>
                      <div class="modal-dialog modal-lg" role=document>
                          <div class="modal-content portfolio-modal">
                              <div class=modal-body>
                                  <div class=row><button class=close type=button aria-label=Close data-dismiss=modal><span aria-hidden=true>×</span></button>
                                      <div class="col-sm-6 col-md-6 col-xs-12 modal_img"><img src=images/item2.jpg class=img-responsive></div>
                                      <div class="col-sm-6 col-md-6 col-xs-12">
                                          <h2>Digital</h2>
                                          <p>Stationery & promotional<br>Website design & build<br>Digital marketing<br>E-commerce</div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-6 col-md-4 thumb">
                      <a href=# data-target=.portfolio-modal3 data-toggle=modal><img src=images/item3.jpg class=img-responsive alt="">
                          <h3>Design For Printing<i>+</i></h3>
                          <p>Stationery & promotional<br>Signage & environment<br>Packaging<br>Additional designs works
                          </p>
                      </a>
                  </div>
                  <div class="fade modal portfolio-modal3" role=dialog aria-labelledby=portfolioModal tabindex=-1>
                      <div class="modal-dialog modal-lg" role=document>
                          <div class="modal-content portfolio-modal">
                              <div class=modal-body>
                                  <div class=row><button class=close type=button aria-label=Close data-dismiss=modal><span aria-hidden=true>×</span></button>
                                      <div class="col-sm-6 col-md-6 col-xs-12 modal_img"><img src=images/item3.jpg class=img-responsive></div>
                                      <div class="col-sm-6 col-md-6 col-xs-12">
                                          <h2>Design For Printing</h2>
                                          <p>Stationery & promotional<br>Signage & environment<br>Packaging<br>Additional designs works</div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  @include('front_pages.teamsection')
  
  <div class="section testimonial_section" id=testimonial_section>
      <div class=container>
          <div class=row>
              <div class=testimonial_area>
                  <h1 class=title>Testimonial</h1>
                  <h3 class=sub_title>The resons you would like to get connect with us!</h3>
                  <div class="owl-carousel owl-theme" id=testimonial_carousel>
                      <div class=item>
                          <blockquote class=block>
                              <p>"What if an artwork is not available, we can do designs and layout services for you."
                                  <footer><cite><span>Century Digital Studio</span></cite></footer>
                          </blockquote>
                      </div>
                      <div class=item>
                          <blockquote class=block>
                              <p>"We offer graphic design and layout services at a very reasonable price."
                                  <footer><cite><span>Century Digital Studio</span></cite></footer>
                          </blockquote>
                      </div>
                      <div class=item>
                          <blockquote class=block>
                              <p>"Build your idea with our inspirations."
                                  <footer><cite><span>Minn Ko Aung | Creative Director, Century Digital Studio</span></cite></footer>
                          </blockquote>
                      </div>
                      <div class=item>
                          <blockquote class=block>
                              <p>"We approach every project as a collaboration, and are most excited when we have the opportunity to learn from you."
                                  <footer><cite><span>Century Digital Studio</span></cite></footer>
                          </blockquote>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="section logo_section">
      <div class=container>
          <div class=row>
              <div class=logo_area>
                  <h2 style=text-align:center>Some Our Clients</h2>
                  <div class="col-md-3 single_logo"><img src=images/bmg-client-logo-01.png></div>
                  <div class="col-md-3 single_logo"><img src=images/pnsp-client-logo-02.png></div>
                  <div class="col-md-3 single_logo"><img src=images/client-logo-03.png></div>
                  <div class="col-md-3 single_logo"><img src=images/client-logo-04.png></div>
              </div>
              <div class=col-md-12 style=padding:20px>
                  <div class="fb-like pull-right" data-share=true data-show-faces=true></div>
              </div>
          </div>
      </div>
  </div>
 
  
@include('front_pages.layouts.footer')
@include('front_pages.layouts.scripts')


