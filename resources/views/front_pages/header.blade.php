<!DOCTYPE html>
<head>
<meta charset=utf-8>
<meta content="IE=edge" http-equiv=X-UA-Compatible>
<meta content="width=device-width,initial-scale=1" name=viewport>
<meta content="Graphic Design, Graphic Design Studio, Graphic Design studio in yangon, Digital Agency, Digital Agency in yangon, Online Marketing, Online Marketing in yangon, Digital Marketing, Digital Marketing in yangon, Printing Services, Printing Services in yangon, One-Stop Printing, One-Stop printing in yangon "
    name=keywords>
<meta content="We are a digital design studio from Yangon, Myanmar. What if an artwork is not available, we can do designs and layout services for you." name=description>
<title>Century | Digital Design Studio</title>
<link href=img/apple-icon-57x57.png rel=apple-touch-icon sizes=57x57>
<link href=img/apple-icon-60x60.png rel=apple-touch-icon sizes=60x60>
<link href=img/apple-icon-72x72.png rel=apple-touch-icon sizes=72x72>
<link href=img/apple-icon-76x76.png rel=apple-touch-icon sizes=76x76>
<link href=img/apple-icon-114x114.png rel=apple-touch-icon sizes=114x114>
<link href=img/apple-icon-120x120.png rel=apple-touch-icon sizes=120x120>
<link href=img/apple-icon-144x144.png rel=apple-touch-icon sizes=144x144>
<link href=img/apple-icon-152x152.png rel=apple-touch-icon sizes=152x152>
<link href=img/apple-icon-180x180.png rel=apple-touch-icon sizes=180x180>
<link href=img/android-icon-192x192.png rel=icon sizes=192x192 type=image/png>
<link href=img/favicon-32x32.png rel=icon sizes=32x32 type=image/png>
<link href=img/favicon-96x96.png rel=icon sizes=96x96 type=image/png>
<link href=img/favicon-16x16.png rel=icon sizes=16x16 type=image/png>
<link href=/manifest.json rel=manifest>
<meta content=#ffffff name=msapplication-TileColor>
<meta content=img/ms-icon-144x144.png name=msapplication-TileImage>
<meta content=#ffffff name=theme-color>
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel=stylesheet>
<link href="https://fonts.googleapis.com/css?family=Lato" rel=stylesheet>
<script src=https://use.fontawesome.com/a981d0c15a.js></script>
<link href=css/owl.carousel.css rel=stylesheet>
<link href=css/owl.theme.css rel=stylesheet>
<link href=css/owl.transitions.css rel=stylesheet>
<link href=css/bootstrap.min.css rel=stylesheet>
<link href=css/animate.css rel=stylesheet>
<link href=css/style.css rel=stylesheet>
<link href=css/responsive.css rel=stylesheet>
</head>