@extends('backend.backend')
@section('title', '| Article Detail')
@section('content')
                  <div class="container">
                  	<div class="col-md-12">	
                  		<h1>
                  			{{ $article->article_title }}
                  			<div class="pull-right" role="group" > 
                  			 {!! Form::open(['method' => 'DELETE', 'route' => ['posts.delete', $article->id]]) !!}                               
                  			 <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
							           <a href="{{ route('posts.edit', $article->id) }}" class="btn btn-info">Edit</a>
							           {!! Form::submit('Delete', ['class' => 'btn btn-danger','id' => 'carDelete','onsubmit' => 'ConfirmDelete()']) !!}
							         </div>
                  		</h1>
                  		 <hr>
                        <span class="label label-primary"> Author Name : {{ $article->author_name }}</span>
                        @foreach($categories as $category)
                          <span class="label label-success"> Category : {{ $category->category_name }}</span>
                        @endforeach
                        <span class="label label-info"> Last Update :{{ $article->updated_at }}</span >
                        <hr>
                  	</div>
                  	<div class="col-md-12">
                  		<div class="blog_img_wrap img-responsive">
                  		@if( ! empty($article->feature_image))
                             <img src="/uploads/blog_feature_img/{{ $article->feature_image }}" alt="{{ $article->article_title }}" class="img-responsive"  style="height: 300px; width: 100%;"/>
                        @else
                             <img src="{{asset('images/cars/03.jpg')}}" class="img-responsive"/>
                        @endif
                       
                        </div>
                  	</div>
                  	<div class="col-md-12">	
	                  	 <p>{!! $article->article_body !!}</p>
                    </div>
                  </div>
@endsection
