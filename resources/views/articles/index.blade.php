@extends('backend.backend')
@section('content')
    {{-- <div class="container"> --}}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Article List<a href="{{ route('categories.create')}}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Crate Category</a></h3></div>
                        <div class="panel-body">
                            <li style="list-style-type:none;border: 1px solid #eaeaea; padding: 0px 20px; padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered text-center">
                                            <tr class="text-center">
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Title</th>
                                                <th class="text-center">Feature Image</th>
                                                <th class="text-center">Post at</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            @foreach ($get_all_articles as $get_all_article)
                                            <tr>
                                                <td>{{  $get_all_article->id }}</td>
                                                <td>{{  $get_all_article->article_title }}</td>
                                                <td>{{  $get_all_article->feature_image }}</td>
                                                <td>{{  $get_all_article->updated_at }}</td>
                                                {{ Form::open([ 'method' => 'delete', 'route' => ['posts.destroy', $get_all_article->id ], 'onsubmit' => 'return ConfirmDelete()']) }}
                                                <td><a href="{{ route('posts.edit', $get_all_article->id) }}" class="btn btn-warning" >Edit</a>
                                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                                    {{ Form::close() }}
                                                     <script>
                                                        function ConfirmDelete(){
                                                        return confirm('Are you sure to delete ?');
                                                        }
                                                    </script>



                                                
                                            </tr>
                                            @endforeach
                                            
                                        </table>
                                        
                                    </div>
                                </a>
                                </div>

                            </li>
                        </div>
                    
                    </div>
                    <div class="text-center">
                        {!! $get_all_articles->links() !!}
                    </div>

                </div>
            </div>
{{--         </div> --}}
@endsection
