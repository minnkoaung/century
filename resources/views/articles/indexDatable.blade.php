 @extends('backend.backend')
@section('content')
<div class="col-md-12">
    <h1>Article Lists <a href="{{ URL::to('backend/posts/create') }}" class="btn btn-success btn-lg pull-right">Add New Article</a></h1>
    <hr>
</div>

    <table class="table table-bordered " id="articles-table">
        <thead class="">
            <tr>
                <th>Id</th>
                <th>Article_Title</th>
                <th>Author Name</th>
                <th>Category</th>
                <th>Feature Img</th>
                <th>Last Update</th>
                <th>Actions</th>
            </tr>
        </thead>
</table>
@push('scripts')
<script>
$(function() {
    $('#articles-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('posts.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'article_title', name: 'article_title' },
            { data: 'author_name', name: 'author_name' },
            { data: 'category_id', name: 'category_id' },
            { data: 'feature_image', name: 'feature_image' },
            { data: 'updated_at', name: 'updated_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endpush
@endsection








 