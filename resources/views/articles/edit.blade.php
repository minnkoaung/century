@extends('backend.backend')

@section('title', '| Edit Articles')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h1>Edit Article </h1>
        <hr>
    {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::model($article, array('route' => array('posts.update', $article->id), 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
            {{ Form::label('article_title', 'Article Title') }}
            {{ Form::text('article_title', null, array('class' => 'form-control')) }}<br>
            <br>

            {{ Form::label('author_name', 'Author Name') }}
            {{ Form::text('author_name', null, array('class' => 'form-control' ,'placeholder'=>'Type Your Name')) }}
            <br>
            
            {{ Form::label('category_name', 'Category Name') }}
            <select name="category_id" class="form-control">
		      @foreach($categories as $category)
		      <option value="{{$category->id}}">{{$category->category_name}}</option>
		      @endforeach
		  	</select> 
            <br><br>

            {{ Form::label('feature_image', 'Upload Feature Image') }}
            {{ Form::file('feature_image', null, array('class' => 'form-control')) }}
            <p ><strong class="text-danger">Feature Image must be 720px x 350px Dimentions at last.</strong></p>
            <br>

             {{ Form::label('article_body', 'Article Text') }}
             {{ Form::textarea('article_body', null, array('class' => 'form-control','id'=>'article_body')) }}<br>


            {{ Form::submit('Update Article', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>
		@push('scripts')
		<script src="{{ asset('js/summernote/summernote.js')}}"></script>
		<script>
		$(document).ready(function() {
		  $('#article_body').summernote({
            height: '300px'
          });
		});
		</script>
		@endpush


@endsection
