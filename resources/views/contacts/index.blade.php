@extends('backend.backend')
@section('title', 'Contact List')
@section('content')
    {{-- <div class="container"> --}}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3>Contact List</h3></div>
                    @foreach ($contacts as $contact)
                        <div class="panel-body">
                            <li style="list-style-type:none;border: 1px solid #eaeaea; padding: 0px 20px; padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-12" style="padding-bottom: 15px;">
                                        <p class="teaser"><b>Contact Person</b> : <span><strong class="text-success">{{  $contact->name }}</strong></span> &nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp<b>Contact's Email</b> : <span><strong class="text-success">{{  $contact->email }}</strong></span></p>
                                        <p class="teaser"></p>
                                        <p class="teaser"><b>Contact's Phone</b> : <span><strong class="text-success">{{  $contact->phone }}</strong></span>&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp<b>Contact's Address</b> : <span><strong class="text-success">{{  $contact->address }}</strong></span></p>
                                        <p class="teaser"><b>Contact's Message :</b></p>
                                        <p class="alert alert-info"><strong>{{  $contact->message }}</strong></p>
                                        {{ Form::open([ 'method' => 'delete', 'route' => ['contact-destory', $contact->id ], 'onsubmit' => 'return ConfirmDelete()']) }}
                                        <p>
                                              {{ Form::submit('Delete', ['class' => 'btn btn-danger pull-right']) }}
                                                    {{ Form::close() }}
                                               <script>
                                                        function ConfirmDelete(){
                                                        return confirm('It will no longer to access. Are you sure to delete ?');
                                                        }
                                              </script>
                                        </p>
                                    </div>
                                </a>
                                </div>
                            </li>
                        </div>
                    @endforeach
                    
                    @if($contacts->count() < 1)
    					<div class="jumbotron" style="background: transparent;">
						  <h2 class="text-center text-success"><i class="ti-thumb-up" style="font-size: 4em;"></i><br>There is no new message!</h2>
						</div>
					@endif
                    </div>
                    <div class="text-center">
                        {!! $contacts->links() !!}
                    </div>

                </div>
            </div>
{{--         </div> --}}
@endsection
