<div style="">
	<p style="font-size:2em; font-style: bold; " >You Have New Message from Ontrack's Contact From ! </p>
	<hr>
	<p style="height: 30px; line-height: 40px; background: #eaeaea; color: #000 !important; padding: 10px;">From  : {{ $name }} , {{ $email}} , Mobile : {{ $phone }}</p>
	<p>Customer Address : {{ $address }}</p>
	<p>Message : {{ $contact_form_message}}</p>
</div>
