@extends('backend.backend')

@section('title', '| Create Category')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        <h1>Add New Category</h1>
        <hr>

    {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::open(array('route' => 'categories.store','enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
            {{ Form::label('category_name', 'Categoy Name') }}
            {{ Form::text('category_name', null, array('class' => 'form-control','placeholder'=>'Please Enter Category Name')) }}
            <br>
            {{ Form::submit('Add New Category', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>


@endsection
