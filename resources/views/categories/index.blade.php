@extends('backend.backend')
@section('content')
    {{-- <div class="container"> --}}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Category List <a href="{{ route('categories.create')}}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Crate Category</a></h3></div>
                    
                        <div class="panel-body">
                            <li style="list-style-type:none;border: 1px solid #eaeaea; padding: 0px 20px; padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered text-center">
                                            <tr class="text-center">
                                                <th class="text-center">Category Id</th>
                                                <th class="text-center">Category Name</th>
                                                <th class="text-center">Updated At</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            @foreach ($categories as $category)
                                            <tr>
                                                <td>{{  $category->id }}</td>
                                                <td>{{  $category->category_name }}</td>
                                                <td>{{  $category->updated_at }}</td>
                                                {{ Form::open([ 'method' => 'delete', 'route' => ['categories.destroy', $category->id ], 'onsubmit' => 'return ConfirmDelete()']) }}
                                                <td><a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning"  style="padding: 10px 60px;">Edit</a>
                                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                                    {{ Form::close() }}
                                                     <script>
                                                        function ConfirmDelete(){
                                                        return confirm('Are you sure to delete ?');
                                                        }
                                                    </script>



                                                <!-- <form class="delete" action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <input type="submit" class="delete btn btn-danger" value="Delete">
                                                </form> -->
                                                
                                                
                                            </tr>
                                            @endforeach
                                            
                                        </table>
                                        
                                    </div>
                                </a>
                                </div>

                            </li>
                        </div>
                    
                    </div>
                    <div class="text-center">
                        {!! $categories->links() !!}
                    </div>

                </div>
            </div>
{{--         </div> --}}
@endsection
