@extends('backend.backend')

@section('title', '| Edit Category')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        <h1>Edit Category</h1>
        <hr>

        {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::model($category, array('route' => array('categories.update', $category->id), 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
            {{ Form::label('category_name', 'Categoy Name') }}
            {{ Form::text('category_name', null, array('class' => 'form-control')) }}<br>
            <br>
            {{ Form::submit('Update Category', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>


@endsection
