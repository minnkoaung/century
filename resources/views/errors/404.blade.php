<!DOCTYPE html>
<html>
<head>
    <title>404 Not Found</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container">
		<br><br><br>
		<div class="jumbotron">
			<div class="container">
				<h1 class="text-center"><i class="fa fa-exclamation-triangle text-danger"></i></h1>
				<h1 class="text-center">404 ! Not Found</h1>
				<p class="text-center">You broke the balance of the internet</p>
				<p class="text-center">
					<a href="/" class="btn btn-primary btn-lg">Click To Go Back</a>
				</p>
			</div>
		</div>
	</div>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.js')}}">
</body>
</html>