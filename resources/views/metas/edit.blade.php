@extends('backend.backend')

@section('title', '| Edit Meta')

@section('content')
<div class="row">

    <div class="col-md-8 col-md-offset-2">

        <h1>Edit Meta Tag</h1>
        <hr>
            {{ Form::model($meta, array('route' => array('meta.update', $meta->id), 'method' => 'PATCH')) }}
            <div class="form-group">
                {{ Form::label('keywords', 'Current Keywords') }}
                {{ Form::textarea('keywords', null, array('class' => 'form-control')) }}<br>

                {{ Form::label('descriptions', 'Current Descriptions') }}
                {{ Form::textarea('descriptions', null, array('class' => 'form-control')) }}<br>

                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
    </div>
    </div>
</div>

@endsection