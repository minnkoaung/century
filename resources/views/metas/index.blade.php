@extends('backend.backend')
@section('title', 'Metas')
@section('content')
    {{-- <div class="container"> --}}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3>Metas</h3></div>
                    @foreach ($metas as $meta)
                        <div class="panel-body">
                            <li style="list-style-type:none;border: 1px solid #eaeaea; padding: 0px 20px; padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-success"><b>Current Keywords</b></p>
                                        <p class="teaser">{{  $meta->keywords }}</p>
                                        <hr>
                                        <p class="text-success"><b>Current Descriptions</b></p>
                                        <p class="teaser">{{  $meta->descriptions }}</p>
                                        <hr>
                                        <p><a href="{{ route('meta.edit', $meta->id) }}" class="btn btn-info" role="button" style="padding: 10px 60px;">Edit</a>
                                        <br></p>
                                        
                                    </div>
                                </a>
                                </div>

                            </li>
                        </div>
                    @endforeach
                    </div>
                    <div class="text-center">
                        {!! $metas->links() !!}
                    </div>

                </div>
            </div>
{{--         </div> --}}
@endsection
