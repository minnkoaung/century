@extends('backend.backend')
@section('title', '| Car Detail')
@section('content')
            <div class="col-md-12">	
                <div class="panel panel-default animated bounceInRight">
                    <div class="panel-heading">
                    	<h3>Car Detail <!-- <a href="{{ URL::to('backend/cars/create') }}" class="btn btn-success pull-right"><i class="ti-plus"></i> Add New Car</a> --></h3>
                    </div>
                        <div class="panel-body">
                            <div class="row" style="padding:0px 10px;">
                            	<table class="table table-bordered table-hover">
                            		<tr>
                            			<td>Car Photo</td>
                            			<td>
                            				<div class="col-md-2 imgwrap" style="width: 450px;height: auto;overflow: hidden;">
                                        @if( ! empty($car->car_photo))
                                            <img src="/uploads/cars/{{ $car->car_photo }}" alt="{{ $car->car_title }}" class="img-responsive" />
                                         @else
                                            <img src="{{asset('images/cars/03.jpg')}}" class="img-responsive"/>
                                        @endif
                            			</td>
                            		</tr>
                            		<tr>
                            			<td>Car Name</td>
                            			<td><strong>{{ strtoupper($car->car_names)}}</strong></td>
                            		</tr>
                            		<tr>
                            			<td>Car Model Line</td>
                            			<td><strong>{{ ucfirst($car->car_model_line)}}</strong></td>
                            		</tr>
                            		<tr>
                            			<td>Car Price</td>
                            			<td><strong>{{ $car->car_price}} {{ strtoupper($car->car_currency)}}/ {{ $car->car_price_per_pay}}</strong></td>
                            		</tr>
                            		<tr>
                            			<td>Number of car's doors</td>
                            			<td><strong>{{ $car->car_door}} Doors</strong></td>
                            		</tr>
                            		<tr>
                            			<td>Number of passenger's seats</td>
                            			<td><strong>{{ $car->car_pasenger_seat}} Seats</strong></td>
                            		</tr>
                            		<tr>
                            			<td>Number of Available Laggage Slots</td>
                            			<td><strong>{{ $car->car_available_laggage}} Slots</strong></td>
                            		</tr>
                            		<tr>
                            			<td>Car's Aircon types</td>
                            			<td><strong>{{ ucfirst($car->car_aircon_type)}}</strong></td>
                            		</tr>
                                    <tr>
                                        <td>Car Usage</td>
                                        <td><strong>{{ $car->car_aircon_type}}</strong></td>
                                    </tr>
                            		
                            	</table>

                                {!! Form::open(['method' => 'DELETE', 'route' => ['cars.destroy', $car->id]]) !!}
                                <a href="{{ route('cars.edit', $car->id) }}" class="btn btn-info" role="button">Edit</a>
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger','id' => 'carDelete','onsubmit' => 'ConfirmDelete()']) !!}
	
                            </div>
                        </div>
                   
                </div>
                   

                </div>
         
@endsection
