@extends('backend.backend')
@section('title', '| Car List')
@section('content')
            <div class="col-md-12">	
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<h3>Cars List <a href="{{ URL::to('backend/cars/create') }}" class="btn btn-success pull-right"><i class="ti-plus"></i> Add New Car</a></h3>
                    </div>
                        <div class="panel-body">
                            <div class="row"m style="padding:0px 10px;">
                            	<table class="table table-bordered">
                            		<tr class="warning">
                            			<th class="text-center">Car Name</th>
                            			<th class="text-center">Car Model</th>
                            			<th class="text-center">Car Price </th>
                            			<th class="text-center">Price In Currency</th>
                            			<th class="text-center">Perssenger Seat</th>
                            			<th class="text-center">Car Photo</th>
                            			<th class="text-center">Action</th>
                            		</tr>
                            		@foreach ($cars as $car)
                            		<tr>
                            			<td class="text-center"><strong>{{strtoupper($car->car_names)}}</strong></td>
                            			<td class="text-center">{{$car->car_model_line}}</td>
                            			<td class="text-center">{{$car->car_price}}</td>
                            			<td class="text-center">{{strtoupper($car->car_currency)}}</td>
                            			<td class="text-center">{{$car->car_pasenger_seat}}</td>
                            			<td class="text-center">
                            				<div class="col-md-2 imgwrap" style="width: 150px;height: 100px;overflow: hidden;">
                                        @if( ! empty($car->car_photo))
                                            <img src="/uploads/cars/{{ $car->car_photo }}" alt="{{ $car->car_title }}" class="img-responsive" />
                                         @else
                                            <img src="{{asset('images/cars/03.jpg')}}" class="img-responsive"/>
                                        @endif
                                     
                                    </div>
                            			</td>
                            			<td class="text-center">
                            				<a href="{{ route('cars.show', $car->id) }}" class="btn btn-info"><i class="ti-eye"></i> View More</a>
                            			</td>
                            		</tr>
                            		 @endforeach
                            	</table>
                                 @if($cars->count() < 1)
                                    <div class="jumbotron" style="background: transparent;">
                                      <h2 class="text-center"><i class="ti-car text-danger" style="font-size: 4em;"></i><br>There is <span class="text-danger">"No Car Data"</span> at current!</h2>
                                      <br><hr><br>
                                      <p class="text-center">
                                          <a href="{{ URL::to('backend/cars/create') }}" class="btn btn-success btn-lg" role="button"><i class="ti-plus"></i> Please Add New</a>
                                      </p>
                                      
                                    </div>
                                @endif
                            </div>
                        </div>
                   
                </div>
                    <div class="text-center">
                        {!! $cars->links() !!}
                    </div>

                </div>
         
@endsection
