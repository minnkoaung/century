@extends('backend.backend')

@section('title', '| Edit Car')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        <h1>Edit Car</h1>
        <hr>
		 {{ Form::model($car, array('route' => array('cars.update', $car->id), 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
            {{ Form::label('car_names', 'Car Name') }}
            {{ Form::text('car_names', null, array('class' => 'form-control')) }}<br>
            <br>

            {{ Form::label('car_model_line', 'Car Modle Line') }}
            {{ Form::text('car_model_line', null, array('class' => 'form-control')) }}<br>
            <br>

            {{ Form::label('car_currency', 'Price In Currency') }} <br>
            {{ Form::select('car_currency',['usd' => 'US Dollar','tb' => 'Thai Baht','mmk' => 'Myanmar Kyats']),null}}
            
            <br><br>

            {{ Form::label('car_price', 'Price') }}
            {{ Form::text('car_price', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Price in Number Only')) }}
            <br>

            {{ Form::label('car_price_per_pay', 'Price Per Pay') }} <br>
            {{ Form::select('car_price_per_pay', ['by_month' => 'Month','by_week' => 'Week', 'by_day' => 'Day']) }}
            <br><br>

            {{ Form::label('car_photo', 'Upload Car\'s Image') }}
            {{ Form::file('car_photo', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::label('car_door', 'Number of doors') }}
            {{ Form::text('car_door', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Number of Doors')) }}
            <br>

            {{ Form::label('car_pasenger_seat', 'Number of passenger') }}
            {{ Form::text('car_pasenger_seat', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Number of Pasenger Seat')) }}
            <br>

            {{ Form::label('car_available_laggage', 'Number of available Laggages') }}
            {{ Form::text('car_available_laggage', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Number of Available Laggages')) }}
            <br>

            {{ Form::label('car_aircon_type', 'Car\'s Aircon Type') }}
            {{ Form::text('car_aircon_type', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Car Aircon Type')) }}
            <br>

            {{ Form::label('car_age', 'Car\'s Age') }}
            {{ Form::text('car_age', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Car Age/Usage')) }}
            <br>

            {{ Form::submit('Update Car Info', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>


@endsection
