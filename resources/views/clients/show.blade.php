@extends('backend.backend')
@section('title', '| Client Detail')
@section('content')
                  <div class="container">
                  	<div class="col-md-12">	
                  		<h1>
                  			{{ $client->client_name }}
                  			<div class="pull-right" role="group" > 
                  			 {!! Form::open(['method' => 'DELETE', 'route' => ['clients.delete', $client->id]]) !!}                               
                  			 <a href="{{ route('clients.index') }}" class="btn btn-default">Back</a>
							           <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-info">Edit</a>
							           {!! Form::submit('Delete', ['class' => 'btn btn-danger','id' => 'carDelete','onsubmit' => 'ConfirmDelete()']) !!}
							         </div>
                  		</h1>
                  		 <hr>
                        <span class="label label-primary"> Client Name : {{ $client->client_name }}</span>
                        @foreach($categories as $category)
                          <span class="label label-success"> Business Category : {{ $category->category_name }}</span>
                        @endforeach
                        <span class="label label-info"> Last Update :{{ $client->updated_at }}</span >
                        <hr>
										</div>
										<div class="col-md-12">
											<table class="table table-striped table-border">
												<tr>
													<td> Client Name</td>
													<td> {{ $client->client_name }}</td>
												</tr>
												<tr>
														<td> Client Business Name</td>
														<td> {{ $client->client_business_name }}</td>
													</tr>
													<tr>
															<td> Client Business Category</td>
															<td> {{ $category->category_name }}</td>
														</tr>
														<tr>
																<td> Client Business Contact Number</td>
																<td> {{ $client->client_phone_no }}</td>
															</tr>
											</table>
										</div>
                  </div>
@endsection
