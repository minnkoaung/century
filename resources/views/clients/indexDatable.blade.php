 @extends('backend.backend')
@section('content')
<div class="col-md-12">
    <h1>Clients List<a href="{{ URL::to('backend/clients/create') }}" class="btn btn-success btn-lg pull-right">Add New Client</a></h1>
    <hr>
</div>

    <table class="table table-bordered " id="clients-table">
        <thead class="">
            <tr>
                <th>Name</th>
                <th>Business Name</th>
                <th>Phone No</th>
                <th>Email</th>
                <th>Facebook</th>
                <th>Webiste</th>
                <th>Last Update</th>
                <th>Actions</th>
            </tr>
        </thead>
</table>
@push('scripts')
<script>
$(function() {
    $('#clients-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('clients.data') !!}',
        columns: [
            { data: 'client_name', name: 'client_name' },
            { data: 'client_business_name', name: 'client_business_name' },
            { data: 'client_phone_no', name: 'client_phone_no' },
            { data: 'client_email', name: 'client_email' },
            { data: 'client_facebook_url', name: 'client_facebook_url' },
            { data: 'client_website', name: 'client_website' },
            { data: 'updated_at', name: 'updated_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endpush
@endsection








 