@extends('backend.backend')

@section('title', '| Add New Clients')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h1>Add New Clients </h1>
        <hr>
            
      

    {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::open(array('route' => 'clients.store','enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
            <div class="row">
                <div class="col-md-6">
                        {{ Form::label('client_name', 'Name') }}
                        {{ Form::text('client_name', null, array('class' => 'form-control','placeholder'=>'Please Enter Name')) }}
                </div>
                <div class="col-md-6">
                        {{ Form::label('client_business_name', 'Business Name') }}
                        {{ Form::text('client_business_name', null, array('class' => 'form-control','placeholder'=>'Please Enter Business Name')) }}
                </div>
            </div>
            <br>


            {{ Form::label('category_name', 'Business Category') }}
            <select name="category_id" class="form-control">
		      @foreach($categories as $category)
		      <option value="{{$category->id}}">{{$category->category_name}}</option>
		      @endforeach
		  	</select> 
            <br><br>

            <div class="row">
                    <div class="col-md-6">
                            {{ Form::label('client_phone_no', 'Contact Number') }}
                            {{ Form::text('client_phone_no', null, array('class' => 'form-control','placeholder'=>'Please Enter Contact Number')) }}        
                    </div>
                    <div class="col-md-6">
                            {{ Form::label('client_email', 'Email Address') }}
                            {{ Form::text('client_email', null, array('class' => 'form-control','placeholder'=>'Please Enter Email Address')) }}
                    </div>
            </div>
            <br>

            <div class="row">
                    <div class="col-md-6">
                            {{ Form::label('client_facebook_url', 'Facebook Address') }}
                            {{ Form::text('client_facebook_url', null, array('class' => 'form-control','placeholder'=>'Please Enter Facebook URL')) }}       
                    </div>
                    <div class="col-md-6">
                            {{ Form::label('client_website', 'Website Address') }}
                            {{ Form::text('client_website', null, array('class' => 'form-control','placeholder'=>'Please Enter Facebook URL')) }}
                    </div>
            </div>
            <br>

            {{ Form::submit('Add New Client', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>


@endsection
