@extends('backend.backend')

@section('title', '| Edit  Clients')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h1>Edit Client Info </h1>
        <hr>
    {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::model($client, array('route' => array('clients.update', $client->id), 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
                <div class="form-group ">
                        <div class="row">
                            <div class="col-md-6">
                                    {{ Form::label('client_name', 'Name') }}
                                    {{ Form::text('client_name', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="col-md-6">
                                    {{ Form::label('client_business_name', 'Business Name') }}
                                    {{ Form::text('client_business_name', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <br>
            
            {{ Form::label('client_business_category', 'Business Category') }}
            <select name="category_id" class="form-control">
		      @foreach($categories as $category)
		      <option value="{{$category->id}}">{{$category->category_name}}</option>
		      @endforeach
		  	</select> 
            <br><br>

            <div class="row">
                    <div class="col-md-6">
                            {{ Form::label('client_phone_no', 'Contact Number') }}
                            {{ Form::text('client_phone_no', null, array('class' => 'form-control')) }}        
                    </div>
                    <div class="col-md-6">
                            {{ Form::label('client_email', 'Email Address') }}
                            {{ Form::text('client_email', null, array('class' => 'form-control')) }}
                    </div>
            </div>
            <br>

            <div class="row">
                    <div class="col-md-6">
                            {{ Form::label('client_facebook_url', 'Facebook Address') }}
                            {{ Form::text('client_facebook_url', null, array('class' => 'form-control')) }}       
                    </div>
                    <div class="col-md-6">
                            {{ Form::label('client_website', 'Website Address') }}
                            {{ Form::text('client_website', null, array('class' => 'form-control')) }}
                    </div>
            </div>
            <br>



            {{ Form::submit('Update Clients Info', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>

@endsection
