@section('header')
        @include('backend.layouts.htmlheader')
@show
<body>
<div class="wrapper">
    @section('sidebar')
        @include('backend.layouts.sidebar')
    @show
    
     @if(Session::has('flash_message'))
            <div class="container">      
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em>
                </div>
            </div>
     @endif 

        <div class="row">
            <div class="col-md-8 col-md-offset-2">              
                @include ('errors.list') {{-- Including error file --}}
            </div>
        </div>

    <div class="main-panel">
       @include('backend.layouts.topnav') 
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    
    @section('footer')
        @include('backend.layouts.footer')    
    @show

     @section('scripts')
       @include('backend.layouts.scripts')  
     @show

