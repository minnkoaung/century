<!--   Core JS Files   -->
    <script src="{{ asset('js/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script>
    <script src="{{ asset('js/js/app.js') }}" type="text/javascript"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ asset('js/js/bootstrap-checkbox-radio.js') }}"></script>
	<!--  Charts Plugin -->
    <script src="{{ asset('js/js/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('js/js/bootstrap-notify.js') }}"></script>
    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="{{ asset('js/js/paper-dashboard.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
    
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
    function ConfirmDelete(){
    return confirm('Are you sure?');
    }
</script>
@stack('scripts')
  


</html>
