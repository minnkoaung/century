 <div class="sidebar" data-background-color="black" data-active-color="danger" >

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper ">
            <div class="logo">
                <a href="/backend" class="simple-text">
                   CENTURY ADMIN
                </a>
            </div>

            <ul class="nav animated bounceInLeft">
                <li {{{ (Request::is('backend') ? 'class=active' : '') }}}>
                    <a href="/backend">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li {{{ (Request::is('backend/meta') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/meta')}}">
                        <i class="ti-pin2"></i>
                        <p>Meta</p>
                    </a>
                </li>
                 <li {{{ (Request::is('backend/team_members') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/team_members')}}">
                        <i class="ti-user"></i>
                        <p>Team Member</p>
                    </a>
                </li>
                <li {{{ (Request::is('backend/clients') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/clients')}}">
                        <i class="ti-user"></i>
                        <p>Clients Info</p>
                    </a>
                </li>

                <li {{{ (Request::is('backend/categories') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/categories')}}">
                        <i class="ti-text"></i>
                        <p>Categories</p>
                    </a>
                </li>

                <li {{{ (Request::is('backend/posts') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/posts')}}">
                        <i class="ti-text"></i>
                        <p>Articles</p>
                    </a>
                </li>

                 <li {{{ (Request::is('backend/tasks') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/tasks')}}">
                        <i class="ti-list"></i>
                        <p>Tasks</p>
                    </a>
                </li>


                 <li {{{ (Request::is('backend/invoice') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/invoice')}}">
                        <i class="ti-receipt"></i>
                        <p>Invoice</p>
                    </a>
                </li>

                <li {{{ (Request::is('backend/contact') ? 'class=active' : '') }}}>
                    <a href="{{url('backend/contact-listing')}}">
                        <i class="ti-comments"></i>
                        <p>Message History</p>
                    </a>
                </li>

                
                
            </ul>
    	</div>
    </div>
