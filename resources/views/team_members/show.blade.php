@extends('backend.backend')
@section('title', '| Member Detail Detail')
@section('content')
            <div class="col-md-12">	
                <div class="panel panel-default animated bounceInRight">
                    <div class="panel-heading">
                    	<h3>Member Detail </h3>
                    </div>
                        <div class="panel-body">
                            <div class="row" style="padding:0px 10px;">
                            	<table class="table table-bordered table-hover">
                            		<tr>
                            			<td>Member Photo</td>
                            			<td>
                            				<div class="col-md-2 imgwrap" style="width: 450px;height: auto;overflow: hidden;">
                                        @if( ! empty($team_member->team_member_photo))
                                            <img src="/uploads/team_members/{{ $team_member->team_member_photo }}" alt="{{ $team_member->team_member_name }}" class="img-responsive" />
                                         @else
                                            <img src="{{asset('images/cars/03.jpg')}}" class="img-responsive"/>
                                        @endif
                            			</td>
                            		</tr>
                            		<tr>
                            			<td>Team Member Name</td>
                            			<td><strong>{{ strtoupper($team_member->team_member_name)}}</strong></td>
                            		</tr>
                                    <tr>
                                        <td>Team Member's Role</td>
                                        <td><strong>{{ strtoupper($team_member->team_member_role)}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Team Member Email Address</td>
                                        <td><strong>{{ strtoupper($team_member->team_member_email)}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Team Member Contact Mobile Number</td>
                                        <td><strong>{{ strtoupper($team_member->team_member_mobile_no)}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Team Member's Facebook Url</td>
                                        <td><strong><a href="{{ $team_member->team_member_facebook_url }}">{{ $team_member->team_member_facebook_url }}</a></strong></td>
                                    </tr>
                                    <tr>
                                        <td>Team Member's Twitter Url</td>
                                         <td><strong><a href="{{ $team_member->team_member_twitter_url }}">{{ $team_member->team_member_twitter_url }}</a></strong></td>                              
                                    </tr>
                                    <tr>
                                        <td>Team Member's Linkedin Url</td>
                                         <td><strong><a href="{{ $team_member->team_member_linkedin_url }}">{{ $team_member->team_member_linkedin_url }}</a></strong></td> 
                                       
                                    </tr>

                            		
                            		
                            	</table>

                                {!! Form::open(['method' => 'DELETE', 'route' => ['team_members.destroy', $team_member->id]]) !!}
                                <a href="{{ route('team_members.edit', $team_member->id) }}" class="btn btn-info" role="button">Edit</a>
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger','id' => 'teamMemberDelete','onsubmit' => 'ConfirmDelete()']) !!}
                                <script>
                                        function ConfirmDelete(){
                                        return confirm('Are you sure?');
                                        }
                                </script>
                            </div>
                        </div>
                   
                </div>
                   

                </div>
         
@endsection
