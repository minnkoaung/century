@extends('backend.backend')

@section('title', '| Edit Car')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        <h1>Edit Member Info</h1>
        <hr>
         {{ Form::model($team_member, array('route' => array('team_members.update', $team_member->id), 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
        <div class="form-group ">
            {{ Form::label('team_member_name', 'Name') }}
            {{ Form::text('team_member_name', null, array('class' => 'form-control','placeholder'=>'Please Enter Name')) }}
            <br>

            {{ Form::label('team_member_role', 'Role') }}
            {{ Form::text('team_member_role', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Role')) }}
            <br>

            {{ Form::label('team_member_email', 'Email') }}
            {{ Form::text('team_member_email', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Email Address')) }}
            <br>

            {{ Form::label('team_member_mobile_no', 'Contact No') }}
            {{ Form::text('team_member_mobile_no', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Mobile Number')) }}
            <br>

            {{ Form::label('team_member_facebook_url', 'Facebook Url') }}
            {{ Form::text('team_member_facebook_url', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Facebook Url')) }}
            <br>

            {{ Form::label('team_member_twitter_url', 'Twitter Url') }}
            {{ Form::text('team_member_twitter_url', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Twitter Url')) }}
            <br>


            {{ Form::label('team_member_linkedin_url', 'Twitter Url') }}
            {{ Form::text('team_member_linkedin_url', null, array('class' => 'form-control' ,'placeholder'=>'Please Enter Linkedin Url')) }}
            <br>

            

            {{ Form::label('team_member_photo', 'Upload Tem Member\'s Image') }}
            {{ Form::file('team_member_photo', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::submit('Update Member Info', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>


@endsection
