@extends('backend.backend')
@section('title', '| Team Member List')
@section('content')
            <div class="col-md-12">	
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<h3>Team Member List <a href="{{ URL::to('backend/team_members/create') }}" class="btn btn-success pull-right"><i class="ti-user"></i> Add New Member</a></h3>
                    </div>
                        <div class="panel-body">
                            <div class="row"m style="padding:0px 10px;">
                            	<table class="table table-bordered">
                            		<tr class="warning">
                            			<th class="text-center">Name</th>
                            			<th class="text-center">Role</th>
                            			<th class="text-center">Email </th>
                            			<th class="text-center">Mobile Number</th>                     
                            			<th class="text-center"> Photo</th>
                            			<th class="text-center">Action</th>
                            		</tr>
                            		@foreach ($teamMembers as $teamMember)
                            		<tr>
                            			<td class="text-center"><strong>{{strtoupper($teamMember->team_member_name)}}</strong></td>
                            			<td class="text-center">{{$teamMember->team_member_role}}</td>
                            			<td class="text-center">{{$teamMember->team_member_email}}</td>
                            			<td class="text-center">{{strtoupper($teamMember->team_member_mobile_no)}}</td>
                            			
                            			<td class="text-center">
                            				<div class="col-md-2 imgwrap" style="width: 150px;height: 100px;overflow: hidden;">
                                        @if( ! empty($teamMember->team_member_photo))
                                            <img src="/uploads/team_members/{{ $teamMember->team_member_photo }}" alt="{{ $teamMember->team_member_name }}" class="img-responsive" />
                                         @else
                                            <img src="{{asset('images/cars/03.jpg')}}" class="img-responsive"/>
                                        @endif
                                     
                                    </div>
                            			</td>
                            			<td class="text-center">
                            				<a href="{{ route('team_members.show', $teamMember->id) }}" class="btn btn-info"><i class="ti-eye"></i> View More</a>
                            			</td>
                            		</tr>
                            		 @endforeach
                            	</table>
                                 @if($teamMembers->count() < 1)
                                    <div class="jumbotron" style="background: transparent;">
                                      <h2 class="text-center"><i class="ti-user text-danger" style="font-size: 4em;"></i><br>There is <span class="text-danger">"No Member Data"</span> at current!</h2>
                                      <br><hr><br>
                                      <p class="text-center">
                                          <a href="{{ URL::to('backend/team_members/create') }}" class="btn btn-success btn-lg" role="button"><i class="ti-plus"></i> Please Add New</a>
                                      </p>
                                      
                                    </div>
                                @endif
                            </div>
                        </div>
                   
                </div>
                    <div class="text-center">
                        {!! $teamMembers->links() !!}
                    </div>

                </div>
         
@endsection
