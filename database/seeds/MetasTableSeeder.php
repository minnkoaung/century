<?php

use Illuminate\Database\Seeder;
use App\Meta;

class MetasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Meta::create([
        	'keywords'	=> 'Cars, Rental, Myanmar Car Rental, Car Rental',
        	'descriptions'	=> 'Thi is sample descriptions',
        	]);
    }
}
