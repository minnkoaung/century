<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'client_name' => 'Ajay Kumar',
            'client_business_name' => 'Ontrack Car Rental',
            'client_business_category' => '1',
            'client_phone_no' => '09955175036',
            'client_email' => 'myanmarajay@gmail.com',
            'client_facebook_url' => 'NA',
            'client_website' => 'NA'
        ]);
    }

}
