<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Meta;
use App\Category;
use App\Client;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UsersTableDataSeeder::class);
        $this->call(MetasTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
    }
}
