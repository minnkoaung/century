<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_name');
            $table->string('client_business_name');
            $table->integer('client_business_category');
            $table->string('client_phone_no');
            $table->string('client_email');
            $table->string('client_facebook_url');
            $table->string('client_website');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('clients');
        Schema::table('clients', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
