<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('car_names');
            $table->string('car_model_line');
            $table->string('car_currency');
            $table->unsignedInteger('car_price');
            $table->string('car_price_per_pay');
            $table->string('car_photo')->nullable();
            $table->unsignedInteger('car_door');
            $table->unsignedInteger('car_pasenger_seat');
            $table->unsignedInteger('car_available_laggage');
            $table->string('car_transmission')->nullable();
            $table->string('car_aircon_type')->nullable();
            $table->unsignedInteger('car_age');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
