<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('team_member_name');
            $table->string('team_member_role');
            $table->string('team_member_email');
            $table->string('team_member_mobile_no');
            $table->string('team_member_facebook_url')->nullable();
            $table->string('team_member_twitter_url')->nullable();
            $table->string('team_member_linkedin_url')->nullable();
            $table->string('team_member_photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_members');
    }
}
