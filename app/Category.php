<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = ['category_name'];

    /**
     * Get the articles for the category post.
     */
    public function articles()
    {
        return $this->belongsTo('App\Articles', 'category_id');
    }

    public function clients()
    {
        return $this->belongsTo('App\Clients', 'client_business_category');
    }
    
}
