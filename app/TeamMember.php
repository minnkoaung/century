<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamMember extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $fillable = ['team_member_name','team_member_role','team_member_email','team_member_mobile_no','team_member_facebook_url','team_member_twitter_url','team_member_linkedin_url','team_member_photo'];
}
