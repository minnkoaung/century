<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
     public $fillable = ['keywords','descriptions'];
}
