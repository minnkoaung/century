<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use Image;
use Alert;

class CarController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $cars = Car::orderby('id', 'desc')->paginate(5);
            
            return view('cars.index',compact('cars'));     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //Validating title and body field
        $this->validate($request, [
            'car_names'=>'required|max:100',
            'car_model_line'=>'required|max:100',
            'car_currency'=>'required|max:25',
            'car_price'=>'required|integer|max:25',
            'car_price' =>'required',
            'car_door' => 'required|integer|max:6',
            'car_pasenger_seat' => 'required|integer|max:50',
            ]);
        //put request value in variables
        $car_names              = $request['car_names'];
        $car_model_line         = $request['car_model_line'];
        $car_currency           = $request['car_currency'];
        $car_price              = $request['car_price'];
        $car_price_per_pay      = $request['car_price_per_pay'];
        //$car_photo              = $request['car_photo'];
        $car_door               = $request['car_door'];
        $car_pasenger_seat      = $request['car_pasenger_seat'];
        $car_available_laggage  = $request['car_available_laggage'];
        $car_transmission       = $request['car_transmission'];
        $car_aircon_type        = $request['car_aircon_type'];   
        $car_age                = $request['car_age'];
        //creating the request
        $car = Car::create($request->only('car_names', 'car_model_line','car_currency','car_price','car_price_per_pay','car_door','car_pasenger_seat','car_available_laggage','car_transmission','car_aircon_type','car_age'));
        // Check if file is present
        if( $request->hasFile('car_photo') ) {
            $car_photo     = $request->file('car_photo');
            $filename      = time() . '.' . $car_photo->getClientOriginalExtension();
            // Image::make($post_thumbnail)->resize(600, 600)->save( public_path('uploads/' . $filename ) );
            Image::make($car_photo)->save( public_path('uploads/cars/' . $filename ) );
            // Set post-thumbnail url
            $car->car_photo = $filename;
        }
        $car->save();
        //Display a successful message upon save
        return redirect()->route('cars.index')->with('flash_message', 'Car,'. $car->car_names.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd($id);
        $car = Car::findOrFail($id); //Find post of id = $id
        return view ('cars.show', compact('car'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::findOrFail($id);
        return view('cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validating title and body field
        $this->validate($request, [
            'car_names'=>'required|max:100',
            'car_model_line'=>'required|max:100',
            'car_currency'=>'required|max:25',
            'car_price'=>'required|integer|max:25',
            'car_price' =>'required',
            'car_door' => 'required|integer|max:6',
            'car_pasenger_seat' => 'required|integer|max:50',
            ]);

        $car = Car::findOrFail($id);
        //dd($request->all());
        $car->car_names = $request->input('car_names');
        $car->car_model_line = $request->input('car_model_line');
        $car->car_currency = $request->input('car_currency');
        $car->car_price = $request->input('car_price');
        $car->car_price_per_pay = $request->input('car_price_per_pay');
        $car->car_door = $request->input('car_door');
        $car->car_pasenger_seat = $request->input('car_pasenger_seat');
        $car->car_available_laggage = $request->input('car_available_laggage');
        $car->car_aircon_type = $request->input('car_aircon_type');
        $car->car_age = $request->input('car_age');

        if( $request->hasFile('car_photo') ) {
            $car_photo     = $request->file('car_photo');
            $filename           = time() . '.' . $car_photo->getClientOriginalExtension();
            Image::make($car_photo)->save( public_path('uploads/cars/' . $filename ) );
            $car->car_photo = $filename;
        }
        $car->save();
        return redirect()->route('cars.index', $car->id)->with('flash_message', 'Current car, '. $car->names.' and updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::findOrFail($id);
        $car->delete();

        return redirect()->route('cars.index')
                         ->with('flash_message','Car infomation has been successfully deleted');
    }
}
