<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meta;

class MetaController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metas = Meta::orderby('id', 'desc')->paginate(5); //show only 5 items at a time in descending order
        return view('metas.index', compact('metas'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
       $this->validate($request, [
            'keywords'=>'required',
            'descriptions'=>'required|max:350',
        ]);

        $metas = Meta::findOrFail($id);
        $meta->keywords = $request->input('keywords');
        $meta->keywords = $request->input('descriptions');
        $meta = Meta::create($request->only('keywords', 'descriptions'));
        $meta->save();
        return redirect()->route('metas.index')->with('flash_message', 'Meta,'. $meta->Keyword.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = Meta::findOrFail($id);
        return view('metas.edit', compact('meta'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->input());
        $this->validate($request, [
            'keywords'=>'required',
            'descriptions'=>'required|max:350',
        ]);
        $meta = Meta::findOrFail($id);
        $meta->keywords = $request->input('keywords');
        $meta->descriptions = $request->input('descriptions');
        $meta->save();
        return redirect()->route('meta.index', $meta->id)->with('flash_message', 'Current Meta, '. $meta->keywords.' and '. $meta->descriptions.' updated');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
