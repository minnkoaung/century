<?php
namespace App\Http\Controllers;
use App\TeamMember;
use Illuminate\Http\Request;
use Image;
use Alert;
class TeamMemberController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $teamMembers = TeamMember::orderby('id', 'desc')->paginate(4);
            return view('team_members.index',compact('teamMembers'));     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team_members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //Validating title and body field
        $this->validate($request, [
            'team_member_name'=>'required|max:100',
            'team_member_role'=>'required|max:100',
            'team_member_email'=>'required|max:100',
            'team_member_mobile_no'=>'required|max:17',
            'team_member_facebook_url'=>'required|max:255',
            'team_member_twitter_url'=>'required|max:255',
            'team_member_linkedin_url'=>'required|max:255',
            ]);
        //put request value in variables
        $team_member_name               = $request['team_member_name'];
        $team_member_role               = $request['team_member_role'];
        $team_member_email              = $request['team_member_email'];
        $team_member_mobile_no          = $request['team_member_mobile_no'];
        $team_member_facebook_url       = $request['team_member_facebook_url'];
        $team_member_twitter_url        = $request['team_member_twitter_url'];
        $team_member_linkedin_url       = $request['team_member_linkedin_url'];

        //creating the request
        $team_member = TeamMember::create($request->only('team_member_name', 'team_member_role','team_member_email','team_member_mobile_no','team_member_facebook_url','team_member_twitter_url','team_member_linkedin_url'));
        // Check if file is present
        if( $request->hasFile('team_member_photo') ) {
            $team_member_photo     = $request->file('team_member_photo');
            $filename      = time() . '.' . $team_member_photo->getClientOriginalExtension();
            // Image::make($post_thumbnail)->resize(600, 600)->save( public_path('uploads/' . $filename ) );
            Image::make($team_member_photo)->save( public_path('uploads/team_members/' . $filename ) );
            // Set post-thumbnail url
            $team_member->team_member_photo = $filename;
        }
        $team_member->save();
        //Display a successful message upon save
        return redirect()->route('team_members.index')->with('flash_message', 'Team Memember,'. $team_member->team_member_name.' created');
    }

    public function show($id){
        $team_member = TeamMember::findOrFail($id);
        return view('team_members.show',compact('team_member'));
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team_member = TeamMember::findOrFail($id);
        return view('team_members.edit', compact('team_member'));
    }



    public function update(Request $request, $id) {
         //Validating title and body field
        $this->validate($request, [
            'team_member_name'=>'max:100',
            'team_member_role'=>'max:100',
            'team_member_email'=>'max:100',
            'team_member_mobile_no'=>'max:17',
            'team_member_facebook_url'=>'max:255',
            'team_member_twitter_url'=>'max:255',
            'team_member_linkedin_url'=>'max:255',
            ]);
        //put request value in variables
        $team_member = TeamMember::findOrFail($id);
        //dd($request->all());
        $team_member->team_member_name = $request->input('team_member_name');
        $team_member->team_member_role = $request->input('team_member_role');
        $team_member->team_member_mobile_no = $request->input('team_member_mobile_no');
        $team_member->team_member_facebook_url = $request->input('team_member_facebook_url');
        $team_member->team_member_twitter_url = $request->input('team_member_twitter_url');
        $team_member->team_member_linkedin_url = $request->input('team_member_linkedin_url');
        

        if( $request->hasFile('team_member_photo') ) {
            $team_member_photo     = $request->file('team_member_photo');
            $filename           = time() . '.' . $team_member_photo->getClientOriginalExtension();
            Image::make($team_member_photo)->save( public_path('uploads/team_members/' . $filename ) );
            $team_member->team_member_photo = $filename;
        }
        $team_member->save();
        return redirect()->route('team_members.index', $team_member->id)->with('flash_message', 'Current team_member, '. $team_member->names.' and updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teamMember = TeamMember::findOrFail($id);
        $teamMember->delete();

        return redirect()->route('team_members.index')
                         ->with('flash_message','Member infomation has been successfully deleted');
    }

}