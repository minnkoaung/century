<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use Datatables;
use DB;
use Image;

class ArticleController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('articles.indexDatable',compact('get_all_articles'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $get_all_articles = Article::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        return Datatables::of(Article::query())->addColumn('action', function ($get_all_articles) 
                            {
                                return '<a href="posts/show/'.$get_all_articles->id.'" class="btn btn-md btn-primary"><i class="glyphicon glyphicon-eye-open"></i> View More</a>';
                            })
                            ->editColumn('id', 'ID: {{$id}}')
                            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('articles.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'article_title'=>'required|max:100',
            'author_name'=>'max:100',
            'category_id'=>'required|integer|max:500',
            'article_body' => 'required',
            'feature_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min-width=720,min-height=350'
            ]);
        //put request value in variables
        $article_title       = $request['article_title'];
        $author_name         = $request['author_name'];
        $category_name       = $request['category_id'];
        $article_body        = $request['article_body'];
        //creating the request
        $article = Article::create($request->only('article_title', 'author_name','category_id','article_body'));
        // Check if file is present
        if( $request->hasFile('feature_image') ) {
            $feature_image     = $request->file('feature_image');
            $filename      = time() . '.' . $feature_image->getClientOriginalExtension();
            Image::make($feature_image)->save( public_path('uploads/blog_feature_img/' . $filename ) );
            $article->feature_image = $filename;
        }
        $article->save();
        return redirect()->route('posts.index')->with('flash_message', 'Articles,'. $article->article_title.' created');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories=Category::all();
        $article = Article::findOrFail($id); //Find post of id = $id
        return view ('articles.show', compact('article','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$categories=Category::all();
        $article = Article::findOrFail($id);
        return view('articles.edit',compact('article','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'article_title'=>'required|max:100',
            'author_name'=>'max:100',
            'category_id'=>'required|integer|max:500',
            'article_body' => 'required',
            'feature_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min-width=720,min-height=350'
            ]);
        //put request value in variables
        $article = Article::findOrFail($id);
        $article_title       = $request['article_title'];
        $author_name         = $request['author_name'];
        $category_id       	 = $request['category_id'];
        $article_body        = $request['article_body'];
        //asign to query
        $article->article_title = $request->input('article_title');
        $article->author_name = $request->input('author_name');
        $article->category_id = $request->input('category_id');
        $article->article_body = $request->input('article_body');
        // Check if file is present
        if( $request->hasFile('feature_image') ) {
            $feature_image     = $request->file('feature_image');
            $filename      = time() . '.' . $feature_image->getClientOriginalExtension();
            Image::make($feature_image)->save( public_path('uploads/blog_feature_img/' . $filename ) );
            $article->feature_image = $filename;
        }
        $article->save();
        return redirect()->route('posts.index', $article->id)->with('flash_message', 'Current Article, '. $article->article_title.' been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        //dd($article);
        $article->delete();
        return redirect()->route('posts.index')
                         ->with('flash_message','Article  has been successfully deleted');
    }
}
