<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Booking;
use Mail;
use Session;
use RealRashid\SweetAlert\Facades\Alert;

class BookingController extends Controller
{
    public function sendBooking(Request $request){
    	$this->validate($request,[
    		'email' => 'required|email',
    		'phone' => 'min:5',
    		'booking_message' => 'min:20'
    	]);
    	$bookingMessage = array(
    		'email' => $request->email,
    		'city' => $request->city,
    		'datetime_pick' => $request->datetime_pick,
    		'name' => $request->name,
    		'phone' => $request->phone,
    		'subject' => $request->services_type,
            'body_message' => $request->body_message
    	);
    	Mail::send('emails.booking',$bookingMessage, function($message) use ($bookingMessage){
    		$message->from($bookingMessage['email']);
    		$message->to('ontrackcar@gmail.com');
    		$message->subject($bookingMessage['subject']);       
    	});
        Alert::success('Your Message Has Been Sent!', 'Your message has been sent and we will get back you soon!')->autoclose(3500);
    	return redirect()->route('home');
    }

     

}
