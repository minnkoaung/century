<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meta;
use App\Car;

class FrontPageController extends Controller
{
      public function index() {
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.landing', compact('metas','cars'));
    }

    public function shortTermRental(){
    	$metas = Meta::all();
        $cars = Car::all();
    	return view('front_pages.services_STR', compact('metas','cars'));
    }

    public function tourPackage(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.services_CFS',compact('metas','cars'));
    }

    public function airportTransfer(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.services_APT',compact('metas','cars'));
    }

    public function longTermRental(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.services_LTR',compact('metas','cars'));
    }

    public function personalDriver(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.services_PDS',compact('metas','cars'));
    }

    public function faqs(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.faqs',compact('metas','cars'));
    }

     public function extraAvailability(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.extra_availability',compact('metas','cars'));
    }

    public function termsAndConditions(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.terms_And_Conditions',compact('metas','cars'));
    }

    public function blog(){
        $metas = Meta::all();
        $cars = Car::all();
        return view('front_pages.blog',compact('metas','cars'));
    }


    
}
