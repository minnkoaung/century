<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contact;
use Mail;
use Session;
use RealRashid\SweetAlert\Facades\Alert;


class ContactController extends Controller
{
    public function index()
    {
    		$this->middleware('auth');
            $contacts = Contact::orderby('id', 'desc')->paginate(10);
            return view('contacts.index',compact('contacts'));     
    }

    public function send(Request $request){
    	//Validating title and body field
    	$this->validate($request,[
    		'name' => 'required|max:30',
    		'email' => 'required|max:30',   		
    	]);
    	$name = $request['name'];
        $email= $request['email'];
        $phone= $request['phone'];
        $address= $request['address'];
        $contact_form_message= $request['message'];

        $contactMessage = array(
    		'name' => $request->name,
    		'email' => $request->email,
    		'phone' => $request->phone,
    		'address' => $request->address,
    		'contact_form_message' => $request->message
    	);
    	Mail::send('emails.contact',$contactMessage, function($message) use ($contactMessage){
    		$message->from($contactMessage['email']);
		    $message->to('ontrackcar@gmail.com');
    		$message->subject($contactMessage['contact_form_message']);       
    	});
        $contactMessageToSave = Contact::create($request->all('name', 'email','phone','address','message'));
        $contactMessageToSave->save();
    	Alert::success('Your Message Has Been Sent!', 'Your message has been sent and we will get back you soon!')->autoclose(3500);
    	return redirect()->route('home');
    }

    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();
        return redirect()->route('contact-list')
                         ->with('flash_message','Contact infomation has been successfully deleted');
    }


}
