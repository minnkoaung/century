<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Client;
use App\Category;
use Datatables;
use DB;

class ClientController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clients.indexDatable',compact('get_all_clients'));
    }

     /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
       $get_all_clients = Client::with('category')->select('*'); 
        return Datatables::of(Client::query())->addColumn('action', function ($get_all_clients) 
                            {
                                return '<a href="clients/show/'.$get_all_clients->id.'" class="btn btn-md btn-primary"><i class="glyphicon glyphicon-eye-open"></i> View More</a>';
                            })
                            ->editColumn('id', 'ID: {{$id}}')
                            ->make(true);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('clients.create',compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
            // @Field Name
            // 'client_name'
            // 'client_business_name' 
            // 'client_business_category' 
            // 'client_phone_no' 
            // 'client_email'
            // 'client_facebook_url'
            // 'client_website'
        
        $this->validate($request, [
            'client_name'=>'required|max:100',
            'client_business_name'=>'max:100',
            'category_id'=>'integer|max:500',
            'client_phone_no'=>'required|max:20',
            'client_email'=>'required|email|max:100',
            'client_facebook_url'=>'required|max:70',
            'client_website'=>'required|max:100'
            ]);
        //put request value in variables
        $client_name = $request['client_name'];
        $client_business_name = $request['client_business_name'];
        $client_business_category       = $request['category_id'];
        $client_phone_no = $request['client_phone_no'];
        $client_email = $request['client_email'];
        $client_facebook_url = $request['client_facebook_url'];
        $client_website = $request['client_website'];
        //creating the request
        $client = Client::create($request->only('client_name', 'client_business_name','client_business_category','client_phone_no','client_email','client_facebook_url','client_website'));
        $client->save();
        return redirect()->route('clients.index')->with('flash_message', 'Client,'. $client->client_name.' created');
        
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories=Category::all();
        $client = Client::findOrFail($id); //Find post of id = $id
        return view ('clients.show', compact('client','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$categories=Category::all();
        $client = Client::findOrFail($id);
        return view('clients.edit',compact('client','categories'));
    }

/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // @Field Name
            // 'client_name'
            // 'client_business_name' 
            // 'client_business_category' 
            // 'client_phone_no' 
            // 'client_email'
            // 'client_facebook_url'
            // 'client_website'
        $this->validate($request, [
            'client_name'=>'required|max:100',
            'client_business_name'=>'max:100',
            //'category_id'=>'integer|max:500',
            'client_phone_no'=>'required|max:20',
            'client_email'=>'required|email|max:100',
            'client_facebook_url'=>'required|max:70',
            'client_website'=>'required|max:100'
            ]);
        //put request value in variables
        $client = Client::findOrFail($id);
        $client->client_name                = $request['client_name'];
        $client->client_business_name       = $request['client_business_name'];
        $client->client_business_category   = $request['category_id'];
        $client->client_phone_no            = $request['client_phone_no'];
        $client->client_email               = $request['client_email'];
        $client->client_facebook_url        = $request['client_facebook_url'];
        $client->client_website              = $request['client_website'];
        $client->save();
        return redirect()->route('clients.index', $client->id)->with('flash_message', 'Current Client Info, '. $client->client_name.' been updated');
    }

    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        //dd($article);
        $client->delete();
        return redirect()->route('clients.index')
                         ->with('flash_message','Client Info  has been successfully deleted');
    }



}
