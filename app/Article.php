<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
   public $fillable = ['article_title','author_name','category_id','feature_image','article_body'];
}
