<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $fillable = ['client_name','client_business_name','client_business_category','client_phone_no','client_email','client_facebook_url','client_website'];
}
